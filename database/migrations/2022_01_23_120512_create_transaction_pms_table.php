<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionPmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_pms', function (Blueprint $table) {
            $table->id();
            $table->string('statut_pm');
            $table->string('taux');
            $table->string('montant');
            $table->string('moyen_paiement');
            $table->string('type_pm');
            $table->double('commission');
            $table->string('statut');
            $table->string('type_transaction');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_pms');
    }
}
