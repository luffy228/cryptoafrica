<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionCryptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_cryptos', function (Blueprint $table) {
            $table->id();
            $table->string('statut_api');
            $table->string('montant');
            $table->string('moyen_paiement');
            $table->string('asset');
            $table->double('commission');
            $table->string('statut');
            $table->string('type_transaction');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_cryptos');
    }
}
